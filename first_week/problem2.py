def findVowels(str):
      
    count = 0
      
    vowel = set("aeiouAEIOU")
      
    for letter in str:
      
        if letter in vowel:
            count = count + 1
      
    print("Total vowels: ", count)

print("Enter line to find total vowels: ")
row = str(input())

findVowels(row)
