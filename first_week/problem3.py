print("Enter the line to find in file and substitute: ")
lineToFind = str(input())

print("Enter the line to substitute with: ")
lineToSubstituteWith = str(input())

fin = open("data.txt", "rt")
fout = open("out.txt", "wt")

for line in fin:
		
	fout.write(line.replace(lineToFind, lineToSubstituteWith))

fin.close()
fout.close()
